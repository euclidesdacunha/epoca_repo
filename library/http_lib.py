import requests
import json


def request(url, request_body):
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        }
    response = requests.post(url, data=json.dumps(request_body),
                             headers=headers)
    return dict(response.json())
