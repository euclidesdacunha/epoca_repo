import csv


def write_csv(output, headers, rows):
    with open(output, 'w', newline='') as csvfile:
        fieldnames = headers
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames,
                                dialect='excel', delimiter=';')
        writer.writeheader()
        writer.writerows(rows)
