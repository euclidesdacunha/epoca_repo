from flask import Flask, request
import json
from parser import parser as p

app = Flask(__name__)


@app.route("/", methods=['POST'])
def parse():
    try:
        parser = p.Parser()
        params = request.get_json()
        url_parse = params['url_parse']
        url_base = params['url_base']
        stop_char = params['stop_char']
        select_args = dict(params['select_args'])

        result = parser.parse_url(url_parse=url_parse, url_base=url_base,
                                  stop_char=stop_char, **select_args)
        return json.dumps(result)
    except:
        return json.dumps({"itens": [], "urls": []})


if __name__ == '__main__':
    app.run()
