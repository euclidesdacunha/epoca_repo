from crawler import crawler
import config

craw = crawler.Crawler(start_url=config.start_url, select_args=config.seletor,
                       output=config.output, server_urls=config.servers,
                       threads=config.threads, limit_pages=config.limit_pages)
craw.produce()
