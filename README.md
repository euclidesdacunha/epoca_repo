## epoca_repo

Esse projeto é dividido em duas aplicações, uma é um servidor flask que cuida do parse dos endereços,
retornando elementos de interesse de busca. O outro é o motor do crawler que roda via comando, responsável
por administrar endereços buscados, visitados e persistir em arquivo csv.

#### Instalação

1 - instalar requirements.txt

2 - modificar config de acordo com as preferências de execução
  a) A aplicação de serviço do parser pode ser replicada  em mais de uma instância e configurada.
  b) Preencher output com o "nome_arquivo.csv", para sua criação
  c) Para rodar o crawler até o fim, deve se tirar o imitador de páginas capturadas

#### Testes

$ python -m unittest

#### Execução

Subir servidor do parser $ python parser_app.py

$ python main.py
