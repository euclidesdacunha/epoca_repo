import unittest
from mock import MagicMock
from bs4 import BeautifulSoup
from parser import parser as p


seletor = {"price": {"tag":"div","class_name":"price"},
            "product_name":{"tag":"div","class_name":"productName"}
            }
html = "<html><body><div class='price'>42</div> <div class='productName'>teste</div> <div><a href='https://www.exemplo.com.br/teste1/p'>ok</a></div></body></html>"
url_base ="https://www.exemplo.com.br/"
start_url ="https://www.exemplo.com.br/teste1/p"
stop_char ="/p"

servers = {'http://serverexemplo.com.br/'}
limit_pages = 5
threads = 11
output = None


class ParserTestCase(unittest.TestCase):
    def setUp(self):
        self.seletor = seletor
        self.html = html
        self.html_bfs = BeautifulSoup(html, 'html.parser')
        self.url_base = url_base
        self.start_url = start_url
        self.stop_char = stop_char

        self.parser = p.Parser()

    def test_find_class_price(self):
        text = self.parser.find_class(self.html_bfs, self.seletor["price"])
        self.assertEqual(text, "42")

    def test_find_class_product_name(self):
        text = self.parser.find_class(self.html_bfs,
                                      self.seletor["product_name"])
        self.assertEqual(text, "teste")

    def test_set_links_start(self):
        self.parser.set_links(self.html_bfs, self.url_base)
        self.assertEqual(self.parser.urls[0], self.start_url)

    def test_parse(self):
        self.parser.request = MagicMock(return_value=self.html)
        self.parser.cook_soup = MagicMock(return_value=self.html_bfs)
        result = self.parser.parse_url(self.start_url, self.url_base,
                                       self.stop_char, **self.seletor)
        self.assertEqual(result['itens']['price'], '42')
        self.assertEqual(result['itens']['product_name'], 'teste')


if __name__ == '__main__':
    unittest.main()
