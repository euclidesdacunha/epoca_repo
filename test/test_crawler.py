from crawler import crawler
import unittest
from mock import MagicMock

seletor = {"price": {"tag":"div","class_name":"price"},
            "product_name":{"tag":"div","class_name":"productName"}
            }
html = "<html><body><div class='price'>42</div> <div class='productName'>teste</div> <div><a href='https://www.exemplo.com.br/teste1/p'>ok</a></div></body></html>"
url_base ="https://www.exemplo.com.br/"
start_url ="https://www.exemplo.com.br/teste1/p"
stop_char ="/p"

mock_request = {'url_parse': 'https://www.exemplo.com.br/',
'itens': {'price':'42','url':'https://www.exemplo.com.br/','productName':'teste'},
'urls': ['https://www.exemplo.com.br/teste1/p']
}

mock_itens = {'itens': {'price':'42','url':'https://www.exemplo.com.br/','productName':'teste'}}
servers = {'http://serverexemplo.com.br/'}
limit_pages = 5
threads = 11
output = None


class CrawlerTestCase(unittest.TestCase):
    def setUp(self):
        self.craw = crawler.Crawler(start_url=start_url,
                                    select_args=seletor,
                                    output=output,
                                    server_urls=servers,
                                    threads=threads,
                                    limit_pages=limit_pages)

    def test_limit_beat_limit_one(self):
        self.craw.limit_pages = 1
        self.craw.itens.append({"title": "teste_tag"})
        self.assertEqual(self.craw.limit_beat(), True)

    def test_limit_beat_limit_none(self):
        self.craw.limit_pages = None
        self.assertEqual(self.craw.limit_beat(), False)

    def test_get_server_one(self):
        server = "http://serverexemplo.com.br/"
        self.craw.server_urls = [server]
        self.assertEqual(self.craw.get_server(), server)

    def test_get_url_init(self):
        self.assertEqual(self.craw.get_url(None), start_url)

    def test_worker(self):
        self.craw.request_parse = MagicMock(return_value=mock_request)
        self.craw.worker()
        item = self.craw.itens[0]
        self.assertEqual(item['price'], '42')


if __name__ == '__main__':
    unittest.main()
