import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent


class Parser(object):
    """Essa classe é responsável por retornar elementos de interesse
    em determinado endereco"""

    def __init__(self):
        self.user_agent = UserAgent()
        self.urls = []
        self.itens = {}

    def parse_url(self, url_parse, url_base, stop_char, **kwargs):
        """Melhoria: Implementar metodo que identifique
        <meta property="og:type" content="og:product" />
        ao invés de url que termine com 'p'"""
        result = dict({'url_parse': url_parse})

        response = self.request(url_parse)
        html = self.cook_soup(response)
        self.set_links(html, url_base)

        if url_parse.endswith(stop_char):
            for key, value in kwargs.items():
                self.itens[key] = self.find_class(html, value)

        result['urls'], result['itens'] = self.urls, self.itens

        return result

    def find_class(self, html, selector):
        try:
            value = html.find(selector["tag"],
                              class_=selector["class_name"]).getText()
            return value
        except AttributeError:
            return ''

    def set_links(self, html, url_base):
        for link in html.findAll('a', href=True):
            if link['href'].startswith(url_base):
                self.urls.append(link['href'])

    def request(self, url_parse):
        headers = {'User-Agent': self.user_agent.random}
        return requests.get(url_parse, headers=headers)

    def cook_soup(self, response):
        return BeautifulSoup(response.content, 'html.parser')
