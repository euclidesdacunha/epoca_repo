import threading
import time
import sys
import random
from library import http_lib, csv_lib


class Crawler(object):
    def __init__(self, start_url, select_args, server_urls,
                 output=None, threads=10, limit_pages=None):
        self.start_url = start_url
        self.output = output
        self.select_args = select_args
        self.max_conections = threads
        self.limit_pages = limit_pages

        self.visited = []
        self.itens = []
        self.urls = set()
        self.server_urls = server_urls

    def worker(self):
        url_work = None
        self.get_url(None)
        try:
            while self.urls:
                if self.limit_beat():
                    break
                time.sleep(random.randrange(5))
                url_work = self.get_url(url_work)

                result = self.request_parse(url_work)

                if len(result['itens']) > 0:
                    result['itens']['url'] = url_work
                    self.itens.append(result['itens'])

                self.visited.append(url_work)
                self.urls.update(set(result['urls']) - set(self.visited))

        except KeyboardInterrupt:
            sys.exit()
        except:
            sys.exc_info

    def produce(self):
        list_threads = []
        for k in range(self.max_conections - 1):
            while threading.active_count() > self.max_conections:
                time.sleep(3)
            thread = threading.Thread(target=self.worker)
            list_threads.append(thread)
            thread.start()
        for thread in list_threads:
            thread.join()
        self.craft_output(self.itens[0].keys(), self.itens)

    def request_parse(self, url_work):
        return http_lib.request(self.get_server(),
                                {"url_parse": url_work,
                                 "url_base": self.start_url,
                                 "stop_char": "/p",
                                 "select_args": self.select_args})

    def get_url(self, url):
        if url is None:
            self.urls.add(self.start_url)
            return self.start_url
        return self.urls.pop()

    def get_server(self):
        server_index = random.randrange(len(self.server_urls))
        return self.server_urls[server_index]

    def craft_output(self, headers, rows):
        if self.output is not None:
            csv_lib.write_csv(self.output, headers, rows)

    def limit_beat(self):
        return self.limit_pages is not None \
               and len(self.itens) >= self.limit_pages
